// for debugging
let error = false;

// val must be from 1 to 99
// trying to minimize num
// {
//      "1": {
//          "num": 3, "val": 75
//      }
// }

let val = 0;
let num = 0;
let possible_combinations = {};
let lowest_average = 100;
let lowest_coins = [];
let lowest_data = {};

let tested_values = [];

function calc_average_num(coin1, coin2, coin3, coin4) {
    //region 1 type of coin

// coin1
    for (let i = 1; i < 100/coin1; i++) {
        num = i;
        val = i*coin1;
        if (val < 100) {
            if (possible_combinations[val]) {
                if (num < possible_combinations[val]['num']) {

                    possible_combinations[val] = {"num": num, "val": val};
                }
            }
            else {
                possible_combinations[val] = {"num": num, "val": val};
            }
        }
    }

// coin2
    for (let i = 1; i < 100/coin2; i++) {
        num = i;
        val = i*coin2;
        if (val < 100) {
            if (possible_combinations[val]) {
                if (num < possible_combinations[val]['num']) {

                    possible_combinations[val] = {"num": num, "val": val};
                }
            }
            else {
                possible_combinations[val] = {"num": num, "val": val};
            }
        }
    }

// coin3
    for (let i = 1; i < 100/coin3; i++) {
        num = i;
        val = i*coin3;
        if (val < 100) {
            if (possible_combinations[val]) {
                if (num < possible_combinations[val]['num']) {

                    possible_combinations[val] = {"num": num, "val": val};
                }
            }
            else {
                possible_combinations[val] = {"num": num, "val": val};
            }
        }
    }

// coin4
    for (let i = 1; i < 100/coin4; i++) {
        num = i;
        val = i*coin4;
        if (val < 100) {
            if (possible_combinations[val]) {
                if (num < possible_combinations[val]['num']) {

                    possible_combinations[val] = {"num": num, "val": val};
                }
            }
            else {
                possible_combinations[val] = {"num": num, "val": val};
            }
        }
    }

    //endregion

    // region 2 types of coins

    // coin1 and coin2
    for (let i = 1; i < 100; i++) {
        num = i;
        val = i*coin1;
        for (let j = 1; j < 100; j++) {
            let num1 = num;
            let val1 = val;
            num1 += j;
            val1 += j*coin2;
            if (val1 < 100) {
                if (possible_combinations[val1]) {
                    if (num1 < possible_combinations[val1]['num']) {

                        possible_combinations[val1] = {"num": num1, "val": val1};
                    }
                }
                else {
                    possible_combinations[val1] = {"num": num1, "val": val1};
                }
            }
        }
    }

    // coin1 and coin3
    for (let i = 1; i < 100; i++) {
        num = i;
        val = i*coin1;
        for (let j = 1; j < 100; j++) {
            let num1 = num;
            let val1 = val;
            num1 += j;
            val1 += j*coin3;
            if (val1 < 100) {
                if (possible_combinations[val1]) {
                    if (num1 < possible_combinations[val1]['num']) {

                        possible_combinations[val1] = {"num": num1, "val": val1};
                    }
                }
                else {
                    possible_combinations[val1] = {"num": num1, "val": val1};
                }
            }
        }
    }

    // coin1 and coin4
    for (let i = 1; i < 100; i++) {
        num = i;
        val = i*coin1;
        for (let j = 1; j < 100; j++) {
            let num1 = num;
            let val1 = val;
            num1 += j;
            val1 += j*coin4;
            if (val1 < 100) {
                if (possible_combinations[val1]) {
                    if (num1 < possible_combinations[val1]['num']) {

                        possible_combinations[val1] = {"num": num1, "val": val1};
                    }
                }
                else {
                    possible_combinations[val1] = {"num": num1, "val": val1};
                }
            }
        }
    }

    // coin2 and coin3
    for (let i = 1; i < 100; i++) {
        num = i;
        val = i*coin2;
        for (let j = 1; j < 100; j++) {
            let num1 = num;
            let val1 = val;
            num1 += j;
            val1 += j*coin3;
            if (val1 < 100) {
                if (possible_combinations[val1]) {
                    if (num1 < possible_combinations[val1]['num']) {

                        possible_combinations[val1] = {"num": num1, "val": val1};
                    }
                }
                else {
                    possible_combinations[val1] = {"num": num1, "val": val1};
                }
            }
        }
    }

    // coin2 and coin4
    for (let i = 1; i < 100; i++) {
        num = i;
        val = i*coin2;
        for (let j = 1; j < 100; j++) {
            let num1 = num;
            let val1 = val;
            num1 += j;
            val1 += j*coin4;
            if (val1 < 100) {
                if (possible_combinations[val1]) {
                    if (num1 < possible_combinations[val1]['num']) {

                        possible_combinations[val1] = {"num": num1, "val": val1};
                    }
                }
                else {
                    possible_combinations[val1] = {"num": num1, "val": val1};
                }
            }
        }
    }

    // coin3 and coin4
    for (let i = 1; i < 100; i++) {
        num = i;
        val = i*coin3;
        for (let j = 1; j < 100; j++) {
            let num1 = num;
            let val1 = val;
            num1 += j;
            val1 += j*coin4;
            if (val1 < 100) {
                if (possible_combinations[val1]) {
                    if (num1 < possible_combinations[val1]['num']) {

                        possible_combinations[val1] = {"num": num1, "val": val1};
                    }
                }
                else {
                    possible_combinations[val1] = {"num": num1, "val": val1};
                }
            }
        }
    }

    //endregion

    //region 3 types of coins

    // coin1 and coin2 and coin3
    for (let i = 1; i < 100; i++) {
        num = i;
        val = i*coin1;
        for (let j = 1; j < 100; j++) {
            let num1 = num;
            let val1 = val;
            num1 += j;
            val1 += j*coin2;
            for (let k = 1; k < 100; k++) {
                let num2 = num1;
                let val2 = val1;
                num2 += k;
                val2 += k*coin3;

                if (val2 < 100) {
                    if (possible_combinations[val2]) {
                        if (num2 < possible_combinations[val2]['num']) {
                            possible_combinations[val2] = {"num": num2, "val": val2};
                        }
                    }
                    else {
                        possible_combinations[val2] = {"num": num2, "val": val2};
                    }
                }
            }
        }
    }

    // coin1 and coin2 and coin4
    for (let i = 1; i < 100; i++) {
        num = i;
        val = i*coin1;
        for (let j = 1; j < 100; j++) {
            let num1 = num;
            let val1 = val;
            num1 += j;
            val1 += j*coin2;
            for (let k = 1; k < 100; k++) {
                let num2 = num1;
                let val2 = val1;
                num2 += k;
                val2 += k*coin4;

                if (val2 < 100) {
                    if (possible_combinations[val2]) {
                        if (num2 < possible_combinations[val2]['num']) {

                            possible_combinations[val2] = {"num": num2, "val": val2};
                        }
                    }
                    else {
                        possible_combinations[val2] = {"num": num2, "val": val2};
                    }
                }
            }
        }
    }

    // coin1 and coin3 and coin4
    for (let i = 1; i < 100; i++) {
        num = i;
        val = i*coin1;
        for (let j = 1; j < 100; j++) {
            let num1 = num;
            let val1 = val;
            num1 += j;
            val1 += j*coin3;
            for (let k = 1; k < 100; k++) {
                let num2 = num1;
                let val2 = val1;
                num2 += k;
                val2 += k*coin4;

                if (val2 < 100) {
                    if (possible_combinations[val2]) {
                        if (num2 < possible_combinations[val2]['num']) {

                            possible_combinations[val2] = {"num": num2, "val": val2};
                        }
                    }
                    else {
                        possible_combinations[val2] = {"num": num2, "val": val2};
                    }
                }
            }
        }
    }

    // coin2 and coin3 and coin4
    for (let i = 1; i < 100; i++) {
        num = i;
        val = i*coin2;
        for (let j = 1; j < 100; j++) {
            let num1 = num;
            let val1 = val;
            num1 += j;
            val1 += j*coin3;
            for (let k = 1; k < 100; k++) {
                let num2 = num1;
                let val2 = val1;
                num2 += k;
                val2 += k*coin4;

                if (val2 < 100) {
                    if (possible_combinations[val2]) {
                        if (num2 < possible_combinations[val2]['num']) {

                            possible_combinations[val2] = {"num": num2, "val": val2};
                        }
                    }
                    else {
                        possible_combinations[val2] = {"num": num2, "val": val2};
                    }
                }
            }
        }
    }

    //endregion

    //region 4 types of coins

    // coin1 and coin2 and coin3 and coin4
    for (let i = 1; i < 100; i++) {
        num = i;
        val = i*coin1;
        for (let j = 1; j < 100; j++) {
            let num1 = num;
            let val1 = val;
            num1 += j;
            val1 += j*coin2;
            for (let k = 1; k < 100; k++) {
                let num2 = num1;
                let val2 = val1;
                num2 += k;
                val2 += k*coin3;
                for (let l = 1; l < 100; l++) {
                    let num3 = num2;
                    let val3 = val2;
                    num3 += l;
                    val3 += l*coin4;
                    if (val3 < 100) {
                        if (possible_combinations[val3]) {
                            if (num3 < possible_combinations[val3]['num']) {

                                possible_combinations[val3] = {"num": num3, "val": val3};
                            }
                        }
                        else {
                            possible_combinations[val3] = {"num": num3, "val": val3};
                        }
                    }
                }
            }
        }
    }

    //endregion
}

// must be 1 for 0.01
let coin1 = 1;
tested_values.push(coin1);

for (let coin2 = 1; coin2 < 30; coin2++) {
    for (let coin3 = 1; coin3 < 30; coin3++) {
        for (let coin4 = 1; coin4 < 30; coin4++) {
            calc_average_num(coin1, coin2, coin3, coin4);

            // checks if all values 0.01 through 0.99 are accounted for
            for (let i = 1; i < 100; i++) {
                if (!possible_combinations[i]) {
                    error = true;
                    console.log('ERROR: ' + i);
                }
            }

            if (!error) {
                // gets average coins used for each val, trying to minimize this value
                let average_num = 0;
                for (let i = 1; i < 100; i++) {
                    average_num += possible_combinations[i]['num'];
                }
                if (average_num/99 < lowest_average) {
                    lowest_data = possible_combinations;
                    lowest_coins = [coin1, coin2, coin3, coin4];
                    lowest_average = average_num/99;
                    console.log('\n');
                    console.log(lowest_coins);
                    console.log(lowest_average);
                }
            }

            possible_combinations = {};
        }
    }
}

console.log(lowest_data);
console.log(lowest_coins);
console.log(lowest_average);