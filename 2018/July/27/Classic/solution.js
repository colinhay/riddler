// define size of square grid
const GRID_SIZE = 5;

let grid = {};
let log = [];
let solutionLogs = {};
let alive = true;

// define grid max and min
const X_MAX = GRID_SIZE-1;
const X_MIN = 0;
const Y_MAX = GRID_SIZE-1;
const Y_MIN = 0;

// define hash function
function hash(x, y) {
    return ( x * GRID_SIZE ) + y;
}

// possible moves
function checkPossibleMoves(location) {
    let left = false;
    let right = false;
    let up = false;
    let down = false;
    let leftUp = false;
    let rightUp = false;
    let leftDown = false;
    let rightDown = false;
    let x = location.x;
    let y = location.y;
    let xLeft = x - 3;
    let xRight = x + 3;
    let yUp = y - 3;
    let yDown = y + 3;
    let xLeftUp = x - 2;
    let yLeftUp = y - 2;
    let xRightUp = x + 2;
    let yRightUp = y - 2;
    let xLeftDown = x - 2;
    let yLeftDown = y + 2;
    let xRightDown = x + 2;
    let yRightDown = y + 2;

    if (xRight <= X_MAX) {
        let newLocation = grid[hash(xRight, y)];
        if (!newLocation.visited) {
            right = true;
        }
    }

    if (xLeft >= X_MIN) {
        let newLocation = grid[hash(xLeft, y)];
        if (!newLocation.visited) {
            left = true;
        }
    }

    if (yDown <= Y_MAX) {
        let newLocation = grid[hash(x, yDown)];
        if (!newLocation.visited) {
            down = true;
        }
    }

    if (yUp >= Y_MIN) {
        let newLocation = grid[hash(x, yUp)];
        if (!newLocation.visited) {
            up = true;
        }
    }

    if (xLeftUp >= X_MIN) {
        if (yLeftUp >= Y_MIN) {
            let newLocation = grid[hash(xLeftUp, yLeftUp)];
            if (!newLocation.visited) {
                leftUp = true;
            }
        }
    }

    if (xRightUp <= X_MAX) {
        if (yRightUp >= Y_MIN) {
            let newLocation = grid[hash(xRightUp, yRightUp)];
            if (!newLocation.visited) {
                rightUp = true;
            }
        }
    }

    if (xLeftDown >= X_MIN) {
        if (yLeftDown <= Y_MAX) {
            let newLocation = grid[hash(xLeftDown, yLeftDown)];
            if (!newLocation.visited) {
                leftDown = true;
            }
        }
    }

    if (xRightDown <= X_MAX) {
        if (yRightDown <= Y_MAX) {
            let newLocation = grid[hash(xRightDown, yRightDown)];
            if (!newLocation.visited) {
                rightDown = true;
            }
        }
    }

    // array of only possible moves
    let moves = [
            {'left': left},
            {'right': right},
            {'up': up},
            {'down': down},
            {'leftUp': leftUp},
            {'rightUp': rightUp},
            {'leftDown': leftDown},
            {'rightDown': rightDown}
        ];

    let possibleMoves = [];

    moves.forEach(function(element) {
        if (Object.values(element)[0]) {
            possibleMoves.push(Object.keys(element)[0]);
        }
    });

    /*
    console.log('Can move left: ' + left);
    console.log('Can move right: ' + right);
    console.log('Can move up: ' + up);
    console.log('Can move down: ' + down);
    console.log('Can move left up: ' + leftUp);
    console.log('Can move right up: ' + rightUp);
    console.log('Can move left down: ' + leftDown);
    console.log('Can move right down: ' + rightDown);
     */

    if (possibleMoves.length) {
        return possibleMoves;
    }

    alive = false;
    return false;
}

function randomMove(possibleMoves) {
    if (!possibleMoves) {
        return false;
    }

    current.visited = true;

    let x = current.x;
    let y = current.y;
    let xLeft = x - 3;
    let xRight = x + 3;
    let yUp = y - 3;
    let yDown = y + 3;
    let xLeftUp = x - 2;
    let yLeftUp = y - 2;
    let xRightUp = x + 2;
    let yRightUp = y - 2;
    let xLeftDown = x - 2;
    let yLeftDown = y + 2;
    let xRightDown = x + 2;
    let yRightDown = y + 2;

    let randomPossibleMove = possibleMoves[Math.floor(Math.random() * possibleMoves.length)];

    switch (randomPossibleMove) {
        case 'left':
            current = grid[hash(xLeft, y)];
            if (logMoveType) {
                log.push(randomPossibleMove);
            }
            if (logMoveCoord) {
                log.push({'x': current.x, 'y': current.y});
            }
            break;
        case 'right':
            current = grid[hash(xRight, y)];
            if (logMoveType) {
                log.push(randomPossibleMove);
            }
            if (logMoveCoord) {
                log.push({'x': current.x, 'y': current.y});
            }
            break;
        case 'up':
            current = grid[hash(x, yUp)];
            if (logMoveType) {
                log.push(randomPossibleMove);
            }
            if (logMoveCoord) {
                log.push({'x': current.x, 'y': current.y});
            }
            break;
        case 'down':
            current = grid[hash(x, yDown)];
            if (logMoveType) {
                log.push(randomPossibleMove);
            }
            if (logMoveCoord) {
                log.push({'x': current.x, 'y': current.y});
            }
            break;
        case 'leftUp':
            current = grid[hash(xLeftUp, yLeftUp)];
            if (logMoveType) {
                log.push(randomPossibleMove);
            }
            if (logMoveCoord) {
                log.push({'x': current.x, 'y': current.y});
            }
            break;
        case 'rightUp':
            current = grid[hash(xRightUp, yRightUp)];
            if (logMoveType) {
                log.push(randomPossibleMove);
            }
            if (logMoveCoord) {
                log.push({'x': current.x, 'y': current.y});
            }
            break;
        case 'leftDown':
            current = grid[hash(xLeftDown, yLeftDown)];
            if (logMoveType) {
                log.push(randomPossibleMove);
            }
            if (logMoveCoord) {
                log.push({'x': current.x, 'y': current.y});
            }
            break;
        case 'rightDown':
            current = grid[hash(xRightDown, yRightDown)];
            if (logMoveType) {
                log.push(randomPossibleMove);
            }
            if (logMoveCoord) {
                log.push({'x': current.x, 'y': current.y});
            }
            break;
        default:
            console.log('ERROR!');
    }

    if (debug) {
        console.log('Moved ' + randomPossibleMove + ' to: (' + current.x + ', ' + current.y + ')\n');
    }
    moveCount++;
}

let best = 0;
let solving = 0;
let current;
let moveCount = 1;

let debug = false;
let showSolving = false;
let logMoveType = false;
let logMoveCoord = true;

function run() {
    best = 0;
    solving = 0;
    moveCount = 1;
    grid = {};
    log = [];
    solutionLogs = {};
    alive = true;

    while(Object.keys(solutionLogs).length !== 25) {
        alive = true;
        moveCount = 1;

        // populate the grid object with keys equal to the hash function
        for (let x = 0; x < GRID_SIZE; x++) {
            for (let y = 0; y < GRID_SIZE; y++) {
                grid[hash(x, y)] = {'x': x, 'y': y, 'visited': false};
            }
        }

        // current location to solve
        let start = grid[solving];

        if (debug) {
            console.log('\nStart at: (' + start.x + ', ' + start.y + ')\n');
        }

        current = start;

        if (logMoveCoord) {
            log.push({'x': current.x, 'y': current.y});
        }

        while (alive) {
            randomMove(checkPossibleMoves(current));
        }

        if (moveCount > best) {
            best = moveCount;
        }
        if (showSolving) {
            console.log(solving);
        }
        if (moveCount === 25) {
            solving++;
            solutionLogs[hash(start.x, start.y)] = log;
        }
        log = [];
        if (Object.keys(solutionLogs).length === 25) {
            console.log(solutionLogs);
            console.log('\nSolved for every starting square.');
        }
    }
    // for animation
    for (let i = 0; i < GRID_SIZE*GRID_SIZE; i++) {
        anim[i] = false;
        document.getElementById(String(i)).onmouseover = function() {anim[i] = true};
        document.getElementById(String(i)).onmouseout = function() {anim[i] = false};
        fillCell(i, 0);
    }

    return solutionLogs;
}

let anim = {};

function fillCell(solution, step) {
    let c = document.getElementById(solution);
    let ctx = c.getContext("2d");
    if (step === 0) {
        ctx.fillStyle="white";
        ctx.fillRect(0,0,250,250);
        ctx.strokeStyle="black";
        for (let x = 0; x < GRID_SIZE; x++) {
            for (let y = 0; y < GRID_SIZE; y++) {
                // draw grid
                ctx.beginPath();
                ctx.rect(x*50,y*50,50,50);
                ctx.stroke();
            }
        }
        ctx.fillStyle = "red";
        ctx.beginPath();
        ctx.arc((solutionLogs[solution][step].x*50)+25,(solutionLogs[solution][step].y*50)+25,10,0,2*Math.PI);
        ctx.fill();
        ctx.fillStyle = "black";
        ctx.fillText(step,(solutionLogs[solution][step].x*50)+22,(solutionLogs[solution][step].y*50)+28);
        step++;
    }
    let theInterval = setInterval(function() {
        if (anim[solution]) {
            ctx.fillStyle="white";
            ctx.fillRect(0,0,250,250);
            ctx.strokeStyle="black";
            for (let x = 0; x < GRID_SIZE; x++) {
                for (let y = 0; y < GRID_SIZE; y++) {
                    // draw grid
                    ctx.beginPath();
                    ctx.rect(x*50,y*50,50,50);
                    ctx.stroke();
                }
            }

            for (let i = 0; i < step+1; i++) {
                ctx.fillStyle = "red";
                ctx.strokeStyle = "red";
                // ctx.fillRect((solutionLogs[solution][step].x*50)+1,(solutionLogs[solution][step].y*50)+1,48,48);

                ctx.beginPath();
                ctx.arc((solutionLogs[solution][i].x*50)+25,(solutionLogs[solution][i].y*50)+25,10,0,2*Math.PI);
                ctx.fill();
                ctx.stroke();
                if (solutionLogs[solution][i-1]) {
                    ctx.beginPath();
                    ctx.moveTo((solutionLogs[solution][i-1].x*50)+25,(solutionLogs[solution][i-1].y*50)+25);
                    ctx.lineTo((solutionLogs[solution][i].x*50)+25,(solutionLogs[solution][i].y*50)+25);
                    ctx.stroke();
                }
            }
            for (let i = 0; i < step+1; i++) {
                ctx.fillStyle = "black";
                ctx.fillText(i,(solutionLogs[solution][i].x*50)+22,(solutionLogs[solution][i].y*50)+28);
            }

            if (step === 24) {
                clearInterval(theInterval);
                setTimeout(function(){fillCell(solution, 0)}, 3000);
            }
            else {
                step++;
            }
        }

    }, 1000);
}

run();

